/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.suite;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author MBN USER
 */
@ManagedBean
@ViewScoped
public class IndexMB implements Serializable{
    
    public void obtenerCookie() {
        // get cookies
        HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                System.out.println(cookies[i].getName() + " - " + cookies[i].getValue());
            }
        }
    }
    
    public void redirigirSistemaSGRT() throws IOException {
        InputStream conPropFile = this.getClass().getResourceAsStream("/systemsURL.properties");
        Properties systemsProp = new Properties();
        systemsProp.load(conPropFile);
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        System.out.println("Redirigiendo al sistema SGRT:");
        System.out.println(systemsProp.getProperty("SGRT"));
        context.redirect(systemsProp.getProperty("SGRT"));
    }
    
    public void redirigirSistemaSGNOM() throws IOException {
        InputStream conPropFile = this.getClass().getResourceAsStream("/systemsURL.properties");
        Properties systemsProp = new Properties();
        systemsProp.load(conPropFile);
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        System.out.println("Redirigiendo al sistema SGNOM:");
        System.out.println(systemsProp.getProperty("SGNOM"));
        context.redirect(systemsProp.getProperty("SGNOM"));
    }
    
    public void redirigirSistemaSISAT() throws IOException {
        InputStream conPropFile = this.getClass().getResourceAsStream("/systemsURL.properties");
        Properties systemsProp = new Properties();
        systemsProp.load(conPropFile);
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        System.out.println("Redirigiendo al sistema SISAT:");
        System.out.println(systemsProp.getProperty("SISAT"));
        context.redirect(systemsProp.getProperty("SISAT"));
    }
}
