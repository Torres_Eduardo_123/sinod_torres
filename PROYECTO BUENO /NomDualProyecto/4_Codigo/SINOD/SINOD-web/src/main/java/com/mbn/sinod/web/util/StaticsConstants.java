package com.mbn.sinod.web.util;

public class StaticsConstants {

    //Ruta login
    public static final String RUTA_LOGIN = "config.path.login";
    
    //Validación contrasena
    public static final String REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~'!@#$%?\\\\\\/&*\\]|\\[=()}\"{+_:;,.><'-])(?=\\S+$).{8,2000}$";
    //Validación correo
    public static final String PATRON_CORREO = "^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    //Validación RFC
    public static final String PATRON_RFC_PERSONA_FISICA = "[A-Z]{4}[0-9]{6}[A-Z0-9]{3}";
    public static final String PATRON_RFC_PERSONA_MORAL = "[A-Z]{3}[0-9]{6}[A-Z0-9]{3}";

    public static final String MESSAGES_CLASSPATH = "messages";

    //Roles
    public static final int USUARIO = 1;
    public static final int ADMINISTRADOR = 2;

    //Sesiones
    public static final int MOSTRAR_MSG_SESION_EXPIRADA = 1;
    

    //WS Config property messages
    public static final String WS_URL_HOST = "config.ws.url.host";
    public static final String TOKEN_PATH = "config.ws.token.path";
    public static final String TOKEN_USER = "config.ws.token.user";
    public static final String TOKEN_PWD = "config.ws.token.passwd";
    public static final String TOKEN_CLIENTID = "config.ws.token.clientid";
    public static final String TOKEN_SECRET = "config.ws.token.secret";

    //OAUTH grant types
    public static final String GRANT_PASSWORD = "password";
    public static final String GRANT_REFRESH = "refresh_token";

    //Util Stuff
    public static final String ACCESS_TOKEN = "access_token";
    public static final String ACCESS_VALUE = "value";
    public static final String LOCALE = "es";

    //WS
    public static final String WS_LOGIN = "ws.path.login";
    public static final String WS_LOGOUT = "ws.path.logout";
    public static final String WS_OBETENER_CATALOGO = "ws.path.obtenerCatalogo";
    
    //public static final String WS_OBETENER_EMPLEADOS = "ws.path.obtenerEmpleados";
    //public static final String WS_BUSCAR_EMPLEADOS = "ws.path.buscarEmpleados";
    //public static final String WS_OBTENER_VACACIONES_EMPLEADOS = "ws.path.obtenerVacacionesEmpleado";
    //public static final String WS_OBTENER_VACACIONES_EMPLEADOS_POR_APROBAR = "ws.path.obtenerVacacionesPorAprobar";
    //public static final String WS_GUARDAR_ACTUALIZAR_VACACIONES = "ws.path.guardarActualizarVacaciones";
    //public static final String WS_BUSCAR_EMPLEADOS_AREA ="ws.path.buscarEmpleadosArea";
    //WS Reuniones
    public static final String WS_GUARDAR_REUNION = "ws.path.guardarReunion";
    public static final String WS_ACTUALIZAR_REUNION = "ws.path.actualizarReunion";
    public static final String WS_LISTAR_REUNIONES = "ws.path.listaReuniones";
    public static final String WS_BUSCAR_REUNIONES = "ws.path.buscarReunion";
    public static final String WS_LISTAR_CIUADES_FILTRO = "ws.path.listarCiudades";
    public static final String WS_LISTAR_LUGARES = "ws.path.listaLugares";
    public static final String WS_LISTAR_ULTIMAS_REUNIONES ="ws.path.obtenerUltimasReuniones";

    public static final String WS_LISTAR_ESTADOS = "ws.path.listaEstados"; 
    public static final String WS_BUCAR_MINUTA_POR_ID = "ws.path.buscarMinutasPorId";
    public static final String WS_FILTRO_FECHAS_AREA= "ws.path.filtroFechasArea";
    public static final String WS_BUSCAR_REPORTE_TEMA="ws.path.buscarReporteTema";
    public static final String WS_FILTRO_COMPROMISOS_COLABORADOR="ws.path.filtroCompromisosColaborador";
    public static final String WS_REPORTE_TEMA="ws.path.reporteTema";
    public static final String WS_REPORTE_MINUTAS_AREA="ws.path.reporteMinutasArea";

    

    
    public static final String WS_LISTAR_AGENDA_POR_REUNION ="ws.path.listaAgendaPorReunion";
    public static final String WS_GUARDAR_LUGAR  = "ws.path.guardarLugar";
    public static final String WS_GUARDAR_CIUDAD = "ws.path.guardarCiudad";
    public static final String WS_GUARDAR_AGENDA = "ws.path.guardarAgenda";
    public static final String WS_ELIMINAR_PUNTO_TRATAR = "ws.path.eliminarPuntoTratar";
    public static final String WS_GUARDAR_PUNTO_TRATAR = "ws.path.guardarPuntoTratar";
    public static final String WS_ACTUALIZAR_AGENDA = "ws.path.actualizarAgenda";
    
    public static final String WS_GUARDAR_INVITADOS = "ws.path.guardarInvitados";
    public static final String WS_GUARDAR_INVITADO = "ws.path.guardarInvitado";
    public static final String WS_ELIMINAR_INVITADO = "ws.path.eliminarInvitado";
    public static final String WS_ACTUALIZAR_INVITADO = "ws.path.actualizarInvitado";
    public static final String WS_LISTAR_INVITADOS_POR_REUNION = "ws.path.listaInvitadosPorReunion";
    

    //WS Usuarios
    public static final String WS_OBTENER_USUARIO_ID = "ws.path.obtenerUsuarioPorId";
    
    //WS Registro
    public static final String WS_GUARDAR_ASISTENTES = "ws.path.guardarAsistentes";
    public static final String WS_LISTAR_ASISTENTES = "ws.path.listarAsistentes";
    public static final String WS_GUARDAR_COMENTARIOS_AGENDA = "ws.path.guardarComentariosAgenda";
    public static final String WS_ELIMINAR_COMENTARIOS_AGENDA = "ws.path.eliminarComentarioAgenda";
    public static final String WS_LISTAR_COMENTARIOS_AGENDA = "ws.path.listaComentariosAgenda";
    public static final String WS_GUARDAR_COMENTARIOS_REUNION = "ws.path.guardarComentariosReunion";
    public static final String WS_LISTAR_COMENTARIOS_REUNION = "ws.path.listaComentariosReunion";
    public static final String WS_ELIMINAR_COMENTARIO_REUNION = "ws.path.eliminarComentarioReunion";
    public static final String WS_GUARDAR_COMPROMISOS = "ws.path.guardarCompromisos";
    public static final String WS_LISTAR_COMPROMISOS = "ws.path.listaCompromisosPorIdReunion";
    public static final String WS_ELIMINAR_COMROMISOS = "ws.path.eliminarCompromiso";
    public static final String WS_ACTUALIZAR_COMPROMISO = "ws.path.actualizarCompromiso";
    
    //WS Empleados
    public static final String WS_LISTAR_EMPLEADOS = "ws.path.listaEmpleados";
    public static final String WS_OBTENER_EMPLEADO_ID = "ws.path.obtenerEmpleadoPorId";
    
    public static final String WS_LISTAR_TODOS_EMPELADOS = "ws.path.detalleEmp";
    
    //WS Areas
    public static final String WS_LISTA_AREAS ="ws.path.listaAreas";
    public static final String WS_OBTENER_AREAS_POR_ID ="ws.path.obtenerAreasPorId"; 
    
	//WS Puestos "NUEVOS"
    public static final String WS_LISTA_PUESTOS ="ws.path.listaPuestos";
    public static final String WS_OBTENER_PUESTOS_POR_ID = "ws.path.obtenerPuestosPorId";


    //Reportes Compromisos 
    public static final String WS_REPORTE_COMPROMISOS_POR_DIA="ws.path.reporteCompromisosPorDia";
    public static final String WS_REPORTE_COMPROMISOS_GENERALES="ws.path.reporteCompromisosGenerales";
    public static final String WS_REPORTE_COMPROMISOS_AREA="ws.path.reporteCompromisosArea";

    //ws Acuerdos
    public static final String WS_LISTA_COMPROMISOS_POR_EJECUTOR = "ws.path.listaCompromisosPorEjecutor";
    public static final String WS_LISTA_COMPROMISOS_POR_VALIDADOR = "ws.path.listaCompromisosPorValidador";
    public static final String WS_LISTA_COMPROMISOS_POR_VERIFICADOR = "ws.path.listaCompromisosPorVerificador";
   

    //calendario
    public static final String WS_LISTA_COMPROMISOS = "ws.path.listaCompromisos";
    
    public static final String WS_LISTA_REUNIOES_INVITADOS = "ws.path.listaReunionesInvitado";
    
    
    //chat
    public static final String WS_GUARDAR_CHAT = "ws.path.guardarChat";
    public static final String WS_ACTUALIZAR_CHAT = "ws.path.actualizarChat";
    public static final String WS_OBTENER_CHAT ="ws.path.obtnerChat";
    
    //Banderas
    public static final String ACTIVO = "1";

    //Privileges
    
    //Default
  
    public static final String EXTRANJERA = "Extranjera";
    public static final String RFC_PARAMETROS = "RFC";

    //URL´s
    public static final String CONTEXT_APP = "/SGRH-web";
    public static final String PAG_VACACIONES= "/vacaciones/vacaciones.xhtml?faces-redirect=true";

    //Plantillas reportes
  //  public static final String PLANTILLA_REPORTE_BITACORA = "plantilla_bitacora.jasper";
  
    //Extensiones de archivos 
    public static final String ARCHIVO_PDF = ".pdf";

    //Mensaje
    public static String CORREO_INEXISTENTE = "E009";



}
