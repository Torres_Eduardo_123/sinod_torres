package com.mbn.sinod.web.mb;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author torre
 */
@Named(value = "pruebasMB")
@ViewAccessScoped
public class PruebasMB implements Serializable {
    private Boolean visible; 
    
    @PostConstruct
    public void iniciarVariable(){
        setVisible(false);
    }
    
    public void dialogAprobarTodos(){
        System.out.println("Aprobar");
         RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('confTodos').show();");
    }
    
    /**
     * @return the visible
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }
    
    
}
