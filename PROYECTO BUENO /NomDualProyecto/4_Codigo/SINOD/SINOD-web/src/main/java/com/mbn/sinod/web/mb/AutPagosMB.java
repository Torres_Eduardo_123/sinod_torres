/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.web.mb;

import com.mbn.sinod.model.dto.DetalleEmpDTO;
import com.mbn.sinod.model.dto.EmpDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import com.mbn.sinod.web.client.EmpleadosWSClient;
import com.mbn.sinod.model.entidades.Tsgrhpuestos;
import com.mbn.sinod.web.client.DetalleEmpWSClient;
import com.mbn.sinod.web.client.PuestosWSClient;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.context.RequestContext;
/**
 *
 * @author torre
 */
@Named(value = "autopagosMB")
@ViewAccessScoped
public class AutPagosMB implements Serializable {
    
    private Boolean visible; 
    private Integer mostrarBoton;
    private List<Tsgrhempleados> listaEmpleados;
    private Tsgrhempleados empleados;
    private List<Tsgrhpuestos> listaPuestos;
    private Tsgrhpuestos puestos;

    private EmpDTO todosEmpleados;
    private List<DetalleEmpDTO> todosEmp;
    
    
  @PostConstruct
    public void iniciarVariable() {  
//        setVisible(false);
//        setListaEmpleados(EmpleadosWSClient.listarEmpleados());
//        setEmpleados(EmpleadosWSClient.obtenerEmpleadoPorId(10));
//        setListaPuestos(PuestosWSClient.listarPuestos());
//        setPuestos(PuestosWSClient.obtenerPuestosPorId(1));}
//        setTodosEmpleados(DetalleEmpWSClient.listarEmp());
        setTodosEmp(DetalleEmpWSClient.listarEmp());
        System.out.println("sdjbsdjk" + getTodosEmp().get(0).getNomcompleto());
        //setTodosEmpleados(DetalleEmpWSClient.listarEmp());
    }
    
    public void dialogAprobarTodos(){
        System.out.println("Aprobar");
         RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('confTodos').show();");
    }
    
    
    /**
     * @return the listaEmpleados
     */
    public List<Tsgrhempleados> getListaEmpleados() {
        return listaEmpleados;
    }

    /**
     * @param listaEmpleados the listaEmpleados to set
     */
    public void setListaEmpleados(List<Tsgrhempleados> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }
    
    /**
     * @return the visible
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }
    
    /**
     * @return the mostrarBoton
     */
    public Integer getMostrarBoton() {
        return mostrarBoton;
    }
    
    public void setMostrarBoton(Integer mostrarBoton) {
        this.mostrarBoton = mostrarBoton;
    }
    
    public void dialogoEditarConfig(int valor) throws IOException{
        setVisible(true);
        setMostrarBoton(valor);
        //reload();
        //RequestContext context = RequestContext.getCurrentInstance();
        //context.execute("PF('detalleConfConcepto').show();");
    }

    /**
     * @return the empleados
     */
    public Tsgrhempleados getEmpleados() {
        return empleados;
    }

    /**
     * @param empleados the empleados to set
     */
    public void setEmpleados(Tsgrhempleados empleados) {
        this.empleados = empleados;
    }
    
    /**
     * @return the listaPuestos
     */
    public List<Tsgrhpuestos> getListaPuestos() {
        return listaPuestos;
    }

    /**
     * @param listaPuestos the listaPuestos to set
     */
    public void setListaPuestos(List<Tsgrhpuestos> listaPuestos) {
        this.listaPuestos = listaPuestos;
    }

    /**
     * @return the puestos
     */
    public Tsgrhpuestos getPuestos() {
        return puestos;
    }

    /**
     * @param puestos the puestos to set
     */
    public void setPuestos(Tsgrhpuestos puestos) {
        this.puestos = puestos;
    }
    
     /**
     * @return the todosEmpleados
     */
    public EmpDTO getTodosEmpleados() {
        return todosEmpleados;
    }

    /**
     * @param todosEmpleados the todosEmpleados to set
     */
    public void setTodosEmpleados(EmpDTO todosEmpleados) {
        this.todosEmpleados = todosEmpleados;
    }

    /**
     * @return the todosEmp
     */
    public List<DetalleEmpDTO> getTodosEmp() {
        return todosEmp;
    }

    /**
     * @param todosEmp the todosEmp to set
     */
    public void setTodosEmp(List<DetalleEmpDTO> todosEmp) {
        this.todosEmp = todosEmp;
    }
}