/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.dto.DetalleEmpDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import java.util.List;
import org.hibernate.transform.Transformers;


public class DetalleEmpDAOImpl extends GenericDAOImpl<Tsgrhempleados, Integer> implements DetalleEmpDAO {

    @Override
    public List<DetalleEmpDTO> empleadosPorArea(Integer empleadoId) {
        System.out.println("aca va bien");
        List<DetalleEmpDTO> detalleEmp = (List<DetalleEmpDTO>)
                getSession().createSQLQuery("SELECT * FROM sgnom.buscar_detalle_empleados();")
                    .addScalar("codempleado")
                    .addScalar("despuesto")
                    .addScalar("nomcompleto")
                    .addScalar("desnbarea")
                .setResultTransformer(Transformers.aliasToBean(DetalleEmpDTO.class))
              
                .list();
        System.out.println("SQL todo bien");
        
                return detalleEmp;
                    
    }
//
//    @Override
//    public void aumentarEmpleado(DetalleEmpDTO detalleEmp) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

}
