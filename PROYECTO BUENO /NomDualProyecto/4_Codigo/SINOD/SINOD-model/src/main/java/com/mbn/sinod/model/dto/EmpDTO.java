/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author eduardotorres
 */
public class EmpDTO {
    private List<DetalleEmpDTO> listaEmp;

    /**
     * @return the listaEmp
     */
    public List<DetalleEmpDTO> getListaEmp() {
        return listaEmp;
    }

    /**
     * @param listaEmp the listaEmp to set
     */
    public void setListaEmp(List<DetalleEmpDTO> listaEmp) {
        this.listaEmp = listaEmp;
    }
    
}
