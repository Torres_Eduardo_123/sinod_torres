/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dto;

/**
 *
 * @author eduardotorres
 */
public class DetalleEmpDTO {


    private Integer codempleado;
    private String despuesto;
    private String nomcompleto;
    private String desnbarea;
   
    
    /**
     * @return the codempleado
     */
    public Integer getCodempleado() {
        return codempleado;
    }

    /**
     * @param codempleado the codempleado to set
     */
    public void setCodempleado(Integer codempleado) {
        this.codempleado = codempleado;
    }

    /**
     * @return the despuesto
     */
    public String getDespuesto() {
        return despuesto;
    }

    /**
     * @param despuesto the despuesto to set
     */
    public void setDespuesto(String despuesto) {
        this.despuesto = despuesto;
    }
    
     /**
     * @return the nomcompleto
     */
    public String getNomcompleto() {
        return nomcompleto;
    }

    /**
     * @param nomcompleto the nomcompleto to set
     */
    public void setNomcompleto(String nomcompleto) {
        this.nomcompleto = nomcompleto;
    }

    /**
     * @return the desnbarea
     */
    public String getDesnbarea() {
        return desnbarea;
    }

    /**
     * @param desnbarea the desnbarea to set
     */
    public void setDesnbarea(String desnbarea) {
        this.desnbarea = desnbarea;
    }

        
}
